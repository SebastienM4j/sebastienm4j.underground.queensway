Queensway
=========

Configuration server for externalized configuration in a distributed system.

Powered by [Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/).

Configurations are stored in a [Git backend](https://gitlab.com/SebastienM4j/queensbury).


Build
-----

### As war

To build as an executable war :

```shell
./gradlew assemble
```

The produced war can be located at `build/libs/queensway-{version}.war`.

### As docker image

To build a docker image :

```shell
./gradlew buildDockerImage
```

This produce a docker image with two tags :

* `queensway:{major}.{minor}`
* `queensway:{major}`

With the `latest` flag, a third flag `queensway:latest` is placed.

```shell
./gradlew buildDockerImage -Platest
```

By defaut, the user SSH key (if existing under `{user.home}/.ssh`) is added to the image (for the Git backend authentication).
To choose another SSH key, give the parent directory path with the `sshDir` option.

```shell
./gradlew buildDockerImage -PsshDir=/path/to/ssh/dir
```


Usage
-----

### Run the war

You can deploy the war in any Java applications server or simply run it on the embedded Tomcat server with `java -jar build/libs/queensway-{version}.war` 

### Run a docker container

Run a docker container with : 

```shell
docker run -p 27010:27010 --name queensway queensway:{major}.{minor}
```

For more run options, see the [Dockerfile](Dockerfile).

### Test

Try with some requests :

```shell
curl http://localhost:27010/foo/bar | jq
curl http://localhost:27010/foo-bar.yml
curl http://localhost:27010/foo-bar.properties
```

### Configure an application

[Spring Boot](https://spring.io/projects/spring-boot) applications with Spring Boot Actuator and Spring Config Client in the classpath
only needs to define the configuration server uri in a `bootstrap.yml` file with `spring.cloud.config.uri` property. See the 
[Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/) documentation.

Other applications can request the resources and use the returned configuration.

FROM openjdk:8


ARG WAR_FILE
COPY ${WAR_FILE} queensway.war

COPY build/.ssh/id_rsa /root/.ssh/id_rsa
COPY build/.ssh/known_hosts /root/.ssh/known_hosts


VOLUME /tmp

EXPOSE 27010

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/queensway.war"]

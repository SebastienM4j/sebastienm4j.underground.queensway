package xyz.sebastienm4j.underground.queensway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


@SpringBootApplication
@EnableConfigServer
public class QueenswayApp
{
    public static void main(String... args)
    {
        SpringApplication.run(QueenswayApp.class, args);
    }
}
